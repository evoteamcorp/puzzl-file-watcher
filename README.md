# Puzzl-File-Watcher #

Puzzl-File-Watcher est une pièce du framework [Puzzl](https://bitbucket.org/evoteamcorp/puzzl-core), cette pièce permet de facilement définir des actions lors de l'ajout/modification/suppression de fichiers au sein de votre projet.

### Informations de la pièce ###

* **Nom**: Puzzl-File-Watcher
* **Version**: 0.0.1
* **Date de release**: N/A

### Pré-requis ###

* **Versions de NodeJS / iojs**
* * **NodeJS**: >= 6
* * **iojs**: N/A
* **Dépendances**
* * **chokidar**: ^1.5.0

### Comment contribuer ###

* Ecrire des tests unitaires, car je n'ai aucune idée de comment m'y prendre T_T
* Ne pas hésiter à proposer des améliorations
* Ouvrir un ticket en cas de bug

### Qui est derrière Puzzl-File-Watcher et comment me contacter ###

* Anthony Briand (acidspike) <ace130210@gmail.com>