import {IPiece, Piece, Injector, ConfigsCore, Injectable, InjectableDefinition, InjectableOptions} from "puzzl-core/Exports";
import {WatchManager} from "./managers/WatchManager";

@Injector()
@Piece()
@Injectable(
    {
        injectables: [
            {
                category: "Manager",
                path    : __dirname + "/managers"
            }
        ] as Array<InjectableDefinition>
    } as InjectableOptions
)

export class FileWatcher implements IPiece {

    public promise: Promise<void>;

    private _chockidar: any = null;

    constructor(
        instanciated: (value?: any) => void,
        error: (error: any) => void,
        private _configsCore: ConfigsCore,
        private _watchManager: WatchManager
    ) {
        this._chockidar = require("chokidar");
        this._configsCore.loadPieceConfigs("FileWatcher").then(instanciated).catch(error);
    }

    public initialize(initialized: (value?: any) => void, error: (error?: any) => void): void {
        this._watchManager.setChokidar(this._chockidar);
        initialized();
    }

    public getPath(): string {
        return __dirname;
    }
}