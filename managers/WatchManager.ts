import {IManager} from "puzzl-core/Exports";
import {WatchPathOptions} from "../manual_typings/built_in";
import {LoggerCore, ConfigsCore, MetaObject, Instantiate, Injector} from "puzzl-core/Exports";
import {Stats} from "fs";

@Instantiate()
@MetaObject()
@Injector()
export class WatchManager implements IManager {

    private _watchPaths: Array<WatchPathOptions> = [];
    private _watchingPath: Array<WatchPathOptions> = [];
    private _chokidar: any = null;

    constructor(private _loggerCore: LoggerCore, private _configCore: ConfigsCore) {
    }

    public initialize(): Promise<any> {
        return new Promise(
            (resolve: (value?: any) => void, reject: (err: any) => void) => {
                resolve();
            }
        );
    }

    /**
     * Add a path to watch
     * @param watchOptions
     */
    public addWatchPath(watchOptions: WatchPathOptions): Promise<void> {
        if (!this._checkPath(watchOptions)) {
            this._loggerCore.debug("Add watch path:", watchOptions.path, "with options:", watchOptions);
            this._watchPaths.push(watchOptions);
            return this._loadWatchPath(watchOptions);
        } else {
            this._loggerCore.debug("Watch path:", watchOptions.path, "with options:", watchOptions, "is already in watch");
            return new Promise(
                (resolve: (value?: any) => void) => {
                    resolve();
                }
            );
        }
    }

    /**
     * Set chokidar
     * @param chokidar
     */
    public setChokidar(chokidar: any): void {
        this._chokidar = chokidar;
    }

    /**
     * Load a watch path
     * @param watchOptions
     * @returns {Promise<T>|Promise}
     * @private
     */
    private _loadWatchPath(watchOptions: WatchPathOptions): Promise<void> {
        this._loggerCore.debug("Load watch path:", watchOptions);
        let promiseLoadWatchPath = (resolve: (value?: any) => void, reject: (err: any) => void) => {
            try {
                let defaultConf = this._configCore.get("PuzzlFileWatcher");
                let options     = global._.merge(defaultConf, watchOptions.options);
                let watch       = this._chokidar.watch(watchOptions.path, options);
                if (typeof watchOptions.eventType === "string") {
                    watch.on(
                        watchOptions.eventType, (pathEvent: string, stats: Stats) => {
                            watchOptions.callback(
                                (
                                    watchOptions.eventType as string
                                ), pathEvent, stats
                            );
                        }
                    );
                } else {
                    let events: Array<string> = watchOptions.eventType as Array<string>;
                    events.forEach(
                        (event: string) => {
                            watch.on(
                                event, (pathEvent: string, stats: Stats) => {
                                    watchOptions.callback(event, pathEvent, stats);
                                }
                            );
                        }
                    );
                }
                resolve();
            } catch (e) {
                console.log(e);
                reject(e);
            }
        };

        return new Promise(promiseLoadWatchPath);
    }

    /**
     * Return if the given watchPathOptions is already in watch
     * @param watchOptions
     * @returns {boolean}
     * @private
     */
    private _checkPath(watchOptions: WatchPathOptions): boolean {
        let res = false;
        this._watchingPath.forEach(
            (watch: WatchPathOptions) => {
                if (watch.path === watchOptions.path) {
                    if (global._.isString(watch.eventType) &&
                        global._.isString(watchOptions.eventType) &&
                        watch.eventType ===
                        watchOptions.eventType) {
                        res = true;
                    } else if (global._.isArray(watch.eventType) && global._.isArray(watchOptions.eventType)) {
                        if (global._.isEqual(watch.eventType.sort(), watchOptions.eventType.sort())) {
                            res = true;
                        }
                    }
                }
            }
        );

        return res;
    }
}