"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var Exports_1 = require("puzzl-core/Exports");
var WatchManager = (function () {
    function WatchManager(_loggerCore, _configCore) {
        this._loggerCore = _loggerCore;
        this._configCore = _configCore;
        this._watchPaths = [];
        this._watchingPath = [];
        this._chokidar = null;
    }
    WatchManager.prototype.initialize = function () {
        return new Promise(function (resolve, reject) {
            resolve();
        });
    };
    WatchManager.prototype.addWatchPath = function (watchOptions) {
        if (!this._checkPath(watchOptions)) {
            this._loggerCore.debug("Add watch path:", watchOptions.path, "with options:", watchOptions);
            this._watchPaths.push(watchOptions);
            return this._loadWatchPath(watchOptions);
        }
        else {
            this._loggerCore.debug("Watch path:", watchOptions.path, "with options:", watchOptions, "is already in watch");
            return new Promise(function (resolve) {
                resolve();
            });
        }
    };
    WatchManager.prototype.setChokidar = function (chokidar) {
        this._chokidar = chokidar;
    };
    WatchManager.prototype._loadWatchPath = function (watchOptions) {
        var _this = this;
        this._loggerCore.debug("Load watch path:", watchOptions);
        var promiseLoadWatchPath = function (resolve, reject) {
            try {
                var defaultConf = _this._configCore.get("PuzzlFileWatcher");
                var options = global._.merge(defaultConf, watchOptions.options);
                var watch_1 = _this._chokidar.watch(watchOptions.path, options);
                if (typeof watchOptions.eventType === "string") {
                    watch_1.on(watchOptions.eventType, function (pathEvent, stats) {
                        watchOptions.callback(watchOptions.eventType, pathEvent, stats);
                    });
                }
                else {
                    var events = watchOptions.eventType;
                    events.forEach(function (event) {
                        watch_1.on(event, function (pathEvent, stats) {
                            watchOptions.callback(event, pathEvent, stats);
                        });
                    });
                }
                resolve();
            }
            catch (e) {
                console.log(e);
                reject(e);
            }
        };
        return new Promise(promiseLoadWatchPath);
    };
    WatchManager.prototype._checkPath = function (watchOptions) {
        var res = false;
        this._watchingPath.forEach(function (watch) {
            if (watch.path === watchOptions.path) {
                if (global._.isString(watch.eventType) &&
                    global._.isString(watchOptions.eventType) &&
                    watch.eventType ===
                        watchOptions.eventType) {
                    res = true;
                }
                else if (global._.isArray(watch.eventType) && global._.isArray(watchOptions.eventType)) {
                    if (global._.isEqual(watch.eventType.sort(), watchOptions.eventType.sort())) {
                        res = true;
                    }
                }
            }
        });
        return res;
    };
    WatchManager = __decorate([
        Exports_1.Instantiate(),
        Exports_1.MetaObject(),
        Exports_1.Injector(), 
        __metadata('design:paramtypes', [Exports_1.LoggerCore, Exports_1.ConfigsCore])
    ], WatchManager);
    return WatchManager;
}());
exports.WatchManager = WatchManager;
//# sourceMappingURL=WatchManager.js.map