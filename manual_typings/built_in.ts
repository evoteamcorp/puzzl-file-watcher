import {Stats} from "fs";
import {WatchOptions} from "chokidar";

export interface WatchPathOptions {
    eventType: string|Array<string>;
    options?: WatchOptions;
    path: string;
    callback(eventType: string, file?: string, stat?: Stats): void;
}