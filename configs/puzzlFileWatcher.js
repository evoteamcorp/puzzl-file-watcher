"use strict";
module.exports = {
    awaitWriteFinish: true,
    ignoreInitial: true,
    ignored: /\.ts|\.map$/
};
//# sourceMappingURL=puzzlFileWatcher.js.map