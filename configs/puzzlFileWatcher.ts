export = {
    awaitWriteFinish: true,
    ignoreInitial: true,
    ignored: /\.ts|\.map$/
};